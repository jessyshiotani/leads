import re
from dataclasses import dataclass
from datetime import datetime

from app.configs.database import db
from app.exceptions.exceptions import InvalidPhoneFormat
from sqlalchemy import Column, Integer, String
from sqlalchemy.sql.sqltypes import DateTime


@dataclass
class Leads(db.Model):
    id: int
    name: str
    email: str
    phone: str
    creation_date: DateTime
    last_visit: DateTime
    visits: int

    __tablename__ = "leads"

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)
    email = Column(String, nullable=False, unique=True)
    phone = Column(String, nullable=False, unique=True)
    creation_date = Column(DateTime, nullable=True, default=datetime.now())
    last_visit = Column(DateTime, nullable=True, default=datetime.now())
    visits = Column(Integer, nullable=True, default=1)
    

    @staticmethod
    def validate_phone(data):
        if re.fullmatch('\(\d{2}\)\d{5}\-\d{4}', data['phone']) == None:
            raise InvalidPhoneFormat('Invalid phone number')

    @staticmethod
    def add_visits(filter_by_email):
         filter_by_email.update({Leads.visits: Leads.visits + 1})

    @staticmethod
    def update_last_visit(filter_by_email):
        filter_by_email.update({Leads.last_visit: datetime.now()})
        
       
       
        

   