import sqlalchemy
from app.exceptions.exceptions import InvalidPhoneFormat
from app.models.leads_model import Leads
from flask import Blueprint, current_app, jsonify, request

bp = Blueprint("leads", __name__, url_prefix="/lead")


@bp.route('', methods=['POST'])
def create_lead():
    data = request.json
  
    try:
        lead = Leads(**data)
        Leads.validate_phone(data)
        
        session = current_app.db.session
        session.add(lead)
        session.commit()
        
        return jsonify(lead), 200

    except TypeError as e:
        return {'error': str(e)}, 400

    except InvalidPhoneFormat as e:
        return {'error': str(e)}, 400

    except sqlalchemy.exc.IntegrityError as e:
        return {'error': str(e.orig).split('\n')[0]}, 400


@bp.route('', methods=['GET'])
def get_leads():
    lead = Leads.query.order_by(Leads.visits.asc()).all()
    if lead != []:
        return jsonify(lead), 200
    else:
        return {'error': 'Could not find any data'}, 404


@bp.route('', methods=['PATCH'])
def update_lead():
    data = request.json
    try:
        filter_by_email = Leads.query.filter_by(email=data['email'])
        
        Leads.add_visits(filter_by_email)
        Leads.update_last_visit(filter_by_email)

        filter_by_email.update(data)
        current_app.db.session.commit()
        lead = filter_by_email.one()

        return jsonify(lead), 200

    except TypeError as e:
        return {'error': str(e)}, 400

    except sqlalchemy.exc.NoResultFound as e:
         return {'error': str(e)}, 400


@bp.route('', methods=['DELETE'])
def delete_lead():
    data = request.json
    try: 
        lead = Leads.query.filter_by(email=data['email']).one()
        
        session = current_app.db.session
        session.delete(lead)
        session.commit()
        return jsonify(lead), 204

    except TypeError as e:
        return {'error': str(e)}, 400

    except sqlalchemy.exc.NoResultFound as e:
        return {'error': str(e)}, 400

    
    
